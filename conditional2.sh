#!/bin/bash

# This is another conditional statement but testing if .. else

# Welcome message

echo "Please enter your username"

# User input variable 

read NAME

if [ "$NAME" = "David" ];
then
	echo "Welcome Back David"
else
	echo "You don't seem to have a valid account yet. Please contact support to create one"
fi

