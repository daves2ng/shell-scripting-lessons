#!/bin/bash
# This is a script I wrote as a starter learning shell scripting

NAME="David"
AGE="35"
SEX="Male"
SCHOOL="Ladoke Akintola University of Technology"
WIFE="Eunice"
DAUGHTER="Jemimah"
SURNAME="Olujobi"
OLD_SURNAME="Ogunjobi"
CURRENT_ROLE="Application/Technical Support Engineer"
DESIRE_ROLE="DevOps/Site Reilability Engineer"
COURSE="Electrical and Electronic Engineer"
WORK="Tek Experts Nigeria Limited"


echo "My name is ${NAME} ${SURNAME} and I am currently ${AGE} years old. My gender is ${SEX} and I am proud to be one rather than claiming some stupid freedom. My Surname was actually ${OLD_SURNAME} before legally changing to my current surname of ${SURNAME}
I am a graduate of ${COURSE} from the prestigious ${SCHOOL}.

I am married to ${WIFE} and have a beautiful daughter named ${DAUGHTER}.

I am a Technology enthusiast and currently working as a ${CURRENT_ROLE} with ${WORK}

I actually love this role but aiming to be a ${DESIRE_ROLE} very soon."
