#!/bin/bash

# This is a sample test script I wrote to try and capture some basic user input and feed the user back the basic information

echo "Please enter your details below"

# reading user input

read -p "Enter your first name: " FNAME
read -p "Enter your last Name: " LNAME
read -p "How old are you: " AGE
read -p "What University did you attend: " SCHOOL
read -p "What yead did you graduate: " GRAD
read -p "Who is your best friend?: " FRIEND

# replaying the captured information to the user

echo "Thank you for take time to answer the questions. From the info gathered, your name is $FNAME $LNAME and you are currently $AGE years old. You graduated from $SCHOOL in the year $GRAD. Your best friend is $FRIEND"

echo "Have a nice Day $FNAME"
