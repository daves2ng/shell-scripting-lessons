#!/bin/bash

# sample script to display system info where the script is running

#Some variables
HOSTNAME=
# Tell the user the script is starting

echo "Starting the sysinfo script"

# Display the hostname of the system
echo "The hostname is the system you are on is" ${hostname}

# Display the current date and time when the information was collected
date

# Display the kernel releases of the system followed by the architecture
uname -r
uname -m

# Display the disk usage in a human readable format
df -h

# End the script by letting the user know it is done.
echo "Stopping Script execution"

