#!/bin/bash

# Now we will be going a bit advanced with user input

# First is a prompt for the user to understand

echo "Please enter your full name: "

# then the line to read user input
# this is capturing and storing 2 variables at once and they are FNAME and LNAME

read -p "Enter your first and Last name: " FNAME LNAME
read -p "Enter your current age: " AGE

# feeding back the user with the supplied information

echo "Your name is $FNAME $LNAME and you are $AGE years old"

