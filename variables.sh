#!/bin/bash

# This is a go at variables in this personal training series on shell scripting
# Some variable rules are as follows
# 1. Variable names cannot start with a number
# 2. Curly braces are used when you need to concatenate a variable name with another word
# 3. 


NAME="David"
SPORT="Foot"

echo my name is "$NAME"

echo "The most popular sport is ${SPORT}ball"
